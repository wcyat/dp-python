import cv2
import numpy as np
import os
import sys

# read all the images in images/circle
# use dir list function
# for each image, read it

images_path = sys.argv[1]

if not images_path.endswith('/'):
    images_path = images_path + '/'

images = os.listdir(images_path)

for image in images:
    img = cv2.imread(images_path + image)
 #   cv2.imshow('image', img)
 #   cv2.waitKey(0)

    # Read image
    # img = cv2.imread('images/ball/' + images[0])

    # Resize image to 400 * 400
    img = cv2.resize(img, (400, 400), interpolation=cv2.INTER_AREA)

    # Convert to grayscale
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    # Blur using 3 * 3 kernel
    blurred = cv2.GaussianBlur(gray, (3, 3), 0)

    # Apply Hough transform on the blurred image
    detected_circles = cv2.HoughCircles(blurred,
                                        cv2.HOUGH_GRADIENT, dp=1, minDist=100, param1=100, param2=30, minRadius=100, maxRadius=200)

    if detected_circles is not None:
        circles = np.uint16(np.around(detected_circles))
        for i in circles[0, :]:
            # draw the outer circle
            cv2.circle(img, (i[0], i[1]), i[2], (0, 255, 0), 2)
            # draw the center of the circle
            cv2.circle(img, (i[0], i[1]), 1, (0, 0, 255), 3)
        cv2.imshow(image, img)
        cv2.waitKey(0)
