import cv2

print(cv2.__version__)

image = cv2.imread("image.jpeg")
print(image.shape)
cv2.imshow("image", image)
cv2.waitKey(0) # waits until a key is pressed
cv2.destroyAllWindows() # destroys the window showing image