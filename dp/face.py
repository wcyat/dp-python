import sys
import os

import cv2

from dp.lib.face_detection import detect_anime_face, detect_face

image_dir = sys.argv[1]
if not image_dir.endswith('/'):
    image_dir = image_dir + '/'

images = os.listdir(image_dir)

for image in images:
    cv2.imshow(image, detect_anime_face(cv2.imread(image_dir + image)))
    if cv2.waitKey(0) & 0xFF == ord('q'):
        break
