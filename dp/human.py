"""
detect human from photos
"""
import sys
import cv2
import os

from dp.lib.detect_human import detect_human

images_path = sys.argv[1]
if not images_path.endswith('/'):
    images_path += '/'
images = os.listdir(images_path)

for image in images:
    img = cv2.imread(images_path + image)
    img = detect_human(img)
    cv2.imshow('image', img)
    cv2.waitKey(0)
