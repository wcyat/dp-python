import cv2
import sys

## Read
img = cv2.imread(sys.argv[1])

## convert to hsv
hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)

# mask of purple
mask1 = cv2.inRange(hsv, (120, 94, 0), (145, 255, 255))

# mask of orange
mask2 = cv2.inRange(hsv, (0, 0, 0), (119, 255, 255))

# mask of red
mask3 = cv2.inRange(hsv, (148, 166, 197), (179, 255, 255))

## final mask and masked
mask = cv2.bitwise_or(mask3, mask2)
target = cv2.bitwise_and(img,img, mask=mask)

cv2.imshow('img', target)
cv2.waitKey(0)

# cv2.imwrite("target.png", target)