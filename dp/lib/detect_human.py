import cv2

def detect_human(img):
    """
    detect human from photos
    :param img:
    :return:
    """
    img = cv2.resize(img, (800, 800), interpolation=cv2.INTER_AREA)
    hog = cv2.HOGDescriptor()
    hog.setSVMDetector(cv2.HOGDescriptor_getDefaultPeopleDetector())

    # detect humans in the image
    (humans, _) = hog.detectMultiScale(
        img, winStride=(10, 10), padding=(32, 32), scale=1.1)
    for (x, y, w, h) in humans:
        pad_w, pad_h = int(0.15 * w), int(0.01 * h)
        cv2.rectangle(img, (x + pad_w, y + pad_h),
                      (x + w, y + h), (0, 255, 0), 2)
    return img