# detect face

import cv2


def detect_face(image: cv2.Mat):
    # load image
    image = cv2.resize(image, (400, 400),  interpolation=cv2.INTER_AREA)
    grey = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    # blur
    grey = cv2.GaussianBlur(grey, (5, 5), 0)

    # detect face
    face_cascade = cv2.CascadeClassifier(
        f'{cv2.__path__[0]}/data/haarcascade_frontalface_default.xml')
    faces = face_cascade.detectMultiScale(
        grey, scaleFactor=1.1, minNeighbors=5, minSize=(30, 30), flags=cv2.CASCADE_SCALE_IMAGE)

    if len(faces) > 0:
        # draw rectangle around detected face
        for (x, y, w, h) in faces:
            cv2.rectangle(image, (x, y), (x + w, y + h), (0, 255, 0), 2)

    return image


def detect_anime_face(image: cv2.Mat):
    # load image
    image = cv2.resize(image, (400, 400),  interpolation=cv2.INTER_AREA)
    grey = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    # blur
    grey = cv2.GaussianBlur(grey, (5, 5), 0)

    # detect face
    face_cascade = cv2.CascadeClassifier('models/lbpcascade_animeface.xml')
    faces = face_cascade.detectMultiScale(
        grey, scaleFactor=1.1, minNeighbors=5, minSize=(30, 30), flags=cv2.CASCADE_SCALE_IMAGE)

    if len(faces) > 0:
        # draw rectangle around detected face
        for (x, y, w, h) in faces:
            cv2.rectangle(image, (x, y), (x + w, y + h), (0, 255, 0), 2)
    
    return image

