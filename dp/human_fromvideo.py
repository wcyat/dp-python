import cv2
from dp.lib.detect_human import detect_human

# get image from camera
cap = cv2.VideoCapture(0)
while True:
    _, img = cap.read()
    img = detect_human(img)
    cv2.imshow('image', img)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break
