import cv2
import numpy as np


def check_closed():
    if cv2.getWindowProperty('img', cv2.WND_PROP_VISIBLE) + cv2.getWindowProperty('img2', cv2.WND_PROP_VISIBLE) < 1:
        return True
    else:
        return False


img = cv2.imread('images/hkust.jpg')
img2 = cv2.imread('images/cave.jpg')


cv2.rectangle(img, (int(img.shape[1] / 2 - 50), int(img.shape[0] / 2 - 50)),
              (int(img.shape[1] / 2 + 50), int(img.shape[0] / 2 + 50)), (255, 0, 0), cv2.FILLED)
cv2.circle(img2, (int(img2.shape[1] / 2), int(img2.shape[0] / 2)), 50, (0, 0, 255))
blur = cv2.GaussianBlur(img, (7, 7), cv2.BORDER_DEFAULT)

cv2.imshow('img', img)
cv2.imshow('imgblur', blur)
cv2.imshow('img2', img2)

# first: min value
# second: max value
# lower than min value: not edges
# higher than max value: edges
canny = cv2.Canny(blur, 100, 200)
cv2.imshow('canny', canny)
canny2 = cv2.Canny(img, 100, 200)
cv2.imshow('canny2', canny2)

gray = cv2.cvtColor(blur, cv2.COLOR_BGR2GRAY)
cv2.imshow('gray', gray)

hsv = cv2.cvtColor(blur, cv2.COLOR_BGR2HSV)
cv2.imshow('hsv', hsv)

lower_red = np.array([0, 50, 50])
upper_red = np.array([10, 255, 255])

mask = cv2.inRange(hsv, lower_red, upper_red)
img_result = cv2.bitwise_and(img, img, mask=mask)

cv2.imshow('mask', img_result)

while True:
    if check_closed():
        break
    key = cv2.waitKey(100)
    if key == 27:
        break

cv2.destroyAllWindows()

