import cv2
import sys
import numpy as np
import copy

waitTime = 33

img = cv2.imread(sys.argv[1])

num_of_masks = int(sys.argv[2] if len(sys.argv) > 2 else 2) or 2

default = {
    "HMin": 0,
    "HMax": 179,
    "SMin": 0,
    "SMax": 255,
    "VMin": 0,
    "VMax": 255,
    "Resize": 100,
    "Rotate": 0
}


def rotate_image(image, angle):
    image_center = tuple(np.array(image.shape[1::-1]) / 2)
    rot_mat = cv2.getRotationMatrix2D(image_center, angle, 1.0)
    result = cv2.warpAffine(
        image, rot_mat, image.shape[1::-1], flags=cv2.INTER_LINEAR)
    return result


def nothing(x):
    return


def init(x: int):
    for i in range(x):
        cv2.namedWindow(f"image{i}")
        for value in list(default.keys()):
            cv2.createTrackbar(
                value, f"image{i}", 1 if value == "Resize" else 0,
                179 if value.startswith("H") else 300 if value == "Resize"
                else 360 if value == "Rotate" else 255, nothing)
            if value.endswith("Max") or value.startswith("R"):
                cv2.setTrackbarPos(
                    value, f"image{i}", default[value])
    cv2.namedWindow("final")
    cv2.createTrackbar("Resize", "final", 1, 300, nothing)
    cv2.createTrackbar("Rotate", "final", 0, 360, nothing)
    cv2.setTrackbarPos("Resize", "final", default["Resize"])
    cv2.setTrackbarPos("Rotate", "final", default["Rotate"])


def get_mask(i: int, hsv) -> tuple[cv2.Mat, int, int]:
    values = copy.deepcopy(default)
    for value in list(values.keys()):
        values[value] = cv2.getTrackbarPos(value, f"image{i}")
    lower = np.array([values["HMin"], values["SMin"], values["VMin"]])
    upper = np.array([values["HMax"], values["SMax"], values["VMax"]])
    resize = int(values["Resize"])
    rotate = int(values["Rotate"])
    mask = cv2.inRange(hsv, lower, upper)
    return mask, resize, rotate


def get_masks(x: int) -> list[tuple[cv2.Mat, int, int]]:
    hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
    masks = []
    for i in range(x):
        masks.append(get_mask(i, hsv))
        pass
    return masks


def resize(img, scale_percent):
    width = int(img.shape[1] * scale_percent / 100) or 1
    height = int(img.shape[0] * scale_percent / 100) or 1
    dim = (width, height)
    return cv2.resize(img, dim, interpolation=cv2.INTER_AREA)

init(num_of_masks)

while True:
    ms = get_masks(num_of_masks)
    for i in range(num_of_masks):
        _img = cv2.bitwise_and(img, img, mask=ms[i][0])
        scale_percent = ms[i][1]
        _img = resize(_img, scale_percent)
        _img = rotate_image(_img, ms[i][2])
        cv2.imshow(f'image{i}', _img)
    mask = cv2.bitwise_or(ms[0][0], ms[1][0])
    for i in range(num_of_masks):
        if i > 1:
            mask = cv2.bitwise_or(mask, ms[i][0])
    output = cv2.bitwise_and(img, img, mask=mask)
    output = resize(output, cv2.getTrackbarPos("Resize", "final"))
    output = rotate_image(output, cv2.getTrackbarPos("Rotate", "final"))
    cv2.imshow('final', output)
    # Wait longer to prevent freeze for videos
    if cv2.waitKey(waitTime) & 0xFF == ord('q'):
        break

cv2.destroyAllWindows()
