X: int = None
while X not in range(0, 2000):
   print("Please enter a valid integer from 0 to 1999.")
   try:
      X = int(input("X: "))
   except ValueError:
      pass
print(f'X is {"< 100" if X < 100 else "in 100 - 1000" if X < 1000 else "in 1000 - 2000" if X < 2000 else ">= 2000"}')
