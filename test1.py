def get_integer(prompt: str):
  val: int = None
  while not val:
    try:
      val = int(input(prompt))
    except ValueError:
      pass
  return val

X: int = get_integer("X: ")
Y: int = get_integer("Y: ")

print((X + Y)**2)
